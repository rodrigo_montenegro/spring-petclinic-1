/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.search;

import org.springframework.samples.petclinic.owner.*;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;

/**
 * @author Frank Luzon
 */
@Controller
class SearchController {

	private static final String VIEWS_PETS_SEARCH_FORM = "pets/createSearchPetForm";

	private final PetRepository pets;

	private final OwnerRepository owners;

	public SearchController(PetRepository pets, OwnerRepository owners) {
		this.pets = pets;
		this.owners = owners;
	}

	@ModelAttribute("types")
	public Collection<PetType> populatePetTypes() {
		return this.pets.findPetTypes();
	}

	@InitBinder("pet")
	public void initPetBinder(WebDataBinder dataBinder) {
		dataBinder.setValidator(new PetValidator());
	}

	@GetMapping("/pets/search")
	public String initCreationSearchForm(Map<String, Object> model) {
		model.put("pet", new Pet());
		return VIEWS_PETS_SEARCH_FORM;
	}

	@GetMapping("/pets")
	public String processFindForm(Pet pet, BindingResult result, Map<String, Object> model) {

		// allow parameterless GET request for /owners to return all records
		if (pet.getName() == null) {
			pet.setName(""); // empty string signifies broadest possible search
		}

		// find pets by name
		Collection<Pet> results = this.pets.findByName(pet.getName());
		if (results.isEmpty()) {
			// no pets found
			result.rejectValue("name", "notFound", "not found");
			return "pets/createSearchPetForm";
		}
		else {
			// multiple pets found
			model.put("selections", results);
			return "pets/petsList";
		}
	}

}
